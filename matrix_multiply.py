def dot_product(vector1, vector2):
    total = 0
    for a, b in zip(vector1, vector2):
        total += a * b

    return total

def naive_multiply(matrix1, matrix2):
    if len(matrix1[0]) != len(matrix2):
        raise ValueError()

    matrix = []
    for row in range(len(matrix1)):
        matrix.append([])
        for col in range(len(matrix2)):
            matrix2_col = [matrix2[i][col] for i in range(len(matrix2))]
            value = dot_product(matrix1[row], matrix2_col)
            matrix[row].append(value)

    return matrix

def dnc_multiply(matrix1, matrix2):
    a, b, c, d = quadrants(matrix1)
    e, f, g, h = quadrants(matrix2)

def strassens_multiply(matrix1, matrix2):
    pass


if __name__ == '__main__':
    from random import randint

    matrix1 = [[randint(-9, 9) for j in range(10)] for i in range(10)]
    matrix2 = [[randint(-9, 9) for j in range(10)] for i in range(10)]

    result1 = naive_multiply(matrix1, matrix2)
    print(result1)
