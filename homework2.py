import quick_sort
from sys import argv

with open(argv[1]) as f:
    nums = [int(line) for line in f]

answer1 = quick_sort.sort(nums[:], 'first')
answer2 = quick_sort.sort(nums[:], 'last')
answer3 = quick_sort.sort(nums[:], 'median')

print("Answer to question 1:", answer1)
print("Answer to question 2:", answer2)
print("Answer to question 3:", answer3)
