from random import randint

def sort(list, pivot='random'):
    return recurse(list, 0, len(list), pivot)

def partition(list, i, j, pivot):
    if pivot != 'first':
        if pivot == 'random':
            pivot = randint(i, j - 1)
        elif pivot == 'median':
            first = i
            middle = i + (j - i - 1) // 2
            last = j - 1

            if list[middle] < list[first]:
                first, middle = middle, first
            if list[last] < list[middle]:
                middle, last = last, middle
            if list[middle] < list[first]:
                first, middle = middle, first

            pivot = middle
        elif pivot == 'last':
            pivot = j - 1
        else:
            raise ValueError(
                    'The possible options for pivot are: '
                    'first, median, last, and random')
        list[i], list[pivot] = list[pivot], list[i]
    pivot = i

    for j in range(i + 1, j):
        if list[j] < list[pivot]:
            list[i + 1], list[j] = list[j], list[i + 1]
            i += 1

    list[i], list[pivot] = list[pivot], list[i]
    return i

def recurse(list, i, j, pivot):
    if j - i <= 1:
        return 0

    mid = partition(list, i, j, pivot)
    num =  j - i - 1

    num += recurse(list, i, mid, pivot)
    num += recurse(list, mid + 1, j, pivot)

    return num

if __name__ == '__main__':
    list1 = [randint(-9000, 9000) for i in range(1000)]
    list2 = [i for i in range(21, 40)] + [i for i in range(20, 0, -1)]

    num11 = sort(list1[:])
    num12 = sort(list1[:], 'first')
    num22 = sort(list2[:], 'first') # only use this if the len(list) <= 993
    num21 = sort(list2, 'median')

    #print(list1)
    print(list2)
    print(num11, "r comparisons in a random list of", len(list1))
    print(num12, "f comparisons in a random list of", len(list1))
    print(num21, "m comparisons in a reversed list of", len(list2))
    print(num22, "f comparisons in a reversed list of", len(list2))

    #print(len(list2) ** 2)# / 1.0203040506070808)
    #if num21 < len(list2) ** 2:
        #print(True)
    #else:
        #print(False)
