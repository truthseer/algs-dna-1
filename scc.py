import random

class Point:
    def __init__(self, id):
        self.id = id
        self.state = 0
        self.stack_size = None
        self.head_of = []
        self.tail_of = []

    def __repr__(self):
        try:
            return "P{}".format(self.id)
        except:
            return "P"

    def add_head(self, head):
        self.tail_of.append(head)
        
    def add_tail(self, tail):
        self.head_of.append(tail)

def to_points(directed_edges):
    points = {}
    for id1, id2 in directed_edges:
        if id1 not in points:
            points[id1] = Point(id1)
        if id2 not in points:
            points[id2] = Point(id2)

        point1 = points[id1]
        point2 = points[id2]
        point1.add_head(point2)
        point2.add_tail(point1)
    
    return list(points.values())

def find(points):
    ordered = []
    grouped = []

    for point in points:
        first_dfs(point, ordered)

    for point in reversed(ordered):
        group = second_dfs(point)
        if group:
            grouped.append(group)

    return grouped

def first_dfs(point, ordered):
    stack = []
    try:
        while True:
            if point.state == 1:
                if point.stack_size == len(stack):
                    ordered.append(point)
                    point.stack_size = None
            else:
                point.state = 1
                point.stack_size = len(stack)
                stack.append(point)
                stack.extend(point.head_of)
            point = stack.pop()
    except IndexError: pass

def second_dfs(point):
    group = []
    stack = []
    try:
        while True:
            if point.state == 2:
                if point.stack_size == len(stack):
                    group.append(point)
                    point.stack_size = None
            else:
                point.state = 2
                point.stack_size = len(stack)
                stack.append(point)
                stack.extend(point.tail_of)
            point = stack.pop()
    except IndexError: pass
    return group

def main():
    links = [(1, 9), (1, 2), (3, 1), (4, 2), (4, 5), (5, 6), (5, 8), (7, 3),
            (8, 10), (9, 11), (10, 4), (10, 5), (11, 7), (11, 12), (12, 10)]
    points = to_points(links)
    scc = find(points)
    print(len(scc), "scc's (should be 5)")
    print("sizes should be: 1 1 1 4 5")
    for cc in sorted(scc, key=len):
        print("cc has", len(cc), "elements")

if __name__ == '__main__':
    main()
