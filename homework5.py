from sys import argv
import shortest_paths

def parse(x):
    numbers = x.split(',')
    return tuple(map(int, numbers))

with open(argv[1]) as f:
    graph = {}
    for line in f:
        sline = line.split()
        graph[int(sline[0])] = dict(map(parse, sline[1:]))

path_lengths = shortest_paths.find(graph)
print('answer: ', end='')
print(  path_lengths[7],
        path_lengths[37],
        path_lengths[59],
        path_lengths[82],
        path_lengths[99],
        path_lengths[115],
        path_lengths[133],
        path_lengths[165],
        path_lengths[188],
        path_lengths[197], sep=',')
