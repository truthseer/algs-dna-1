import count_inversions
from sys import argv

with open(argv[1]) as f:
    nums = [int(line) for line in f]
    _, inversions = count_inversions.count(nums)
    print(inversions)
