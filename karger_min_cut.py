import random

class Vertex:
    def __init__(self, id, connections):
        self.id = id
        self.connections = connections

def min_cut(connections):
    num_vertices = len(connections)
    if num_vertices < 2:
        raise ValueError('Need two or more vertices to calculate a min cut')

    runs = 10#4 * num_vertices  # anything >= the number of vertices
    merges = num_vertices - 2  # we need two vertices left to find a min cut
    min_cut = num_vertices - 1  # the maximum number of edges from one vertex

    for run in range(runs):
        if run % 13 == 0: print(run, "runs done -- min cut =", min_cut)

        vertices = to_vertices(connections)
        for merge in range(merges):
            collapse_random_edge(vertices)
        cut = list(vertices.values())[0]
        min_cut = min(min_cut, len(cut.connections))

    print(run, "runs done -- min cut =", min_cut)
    return min_cut

def to_vertices(connections):
    return dict((id, Vertex(id, others[:])) for id, others in connections)

def vertex_ids_of_random_edge(vertices):
    id1 = random.choice(list(vertices.keys()))
    id2 = random.choice(vertices[id1].connections)
    return id1, id2

def collapse_random_edge(vertices):
    id1, id2 = vertex_ids_of_random_edge(vertices)
    new_connections = [i for i in vertices[id2].connections if i != id1]
    del vertices[id2]

    collapse_change(vertices, new_connections, id1, id2)
    collapse_purge(vertices, id1, id2)
    vertices[id1].connections.extend(new_connections)

def collapse_change(vertices, new_connections, id1, id2):
    for id in new_connections:
        for i in range(vertices[id].connections.count(id2)):
            index = vertices[id].connections.index(id2)
            vertices[id].connections[index] = id1

        # half as fast!?
        #for i in range(len(vertices[id].connections)):
            #if vertices[id].connections[i] == id2:
                #vertices[id].connections[i] = id1

def collapse_purge(vertices, id1, id2):
    for i in range(len(vertices[id1].connections) - 1, -1, -1):
        if vertices[id1].connections[i] == id2:
            del vertices[id1].connections[i]

def main():
    links = [(1, [2, 3, 4, 5]), (2, [1, 3, 4]),
             (3, [1, 2, 5]),    (4, [1, 2]),    (5, [1, 3])]
    m = min_cut(links)
    print(m, '(should be 2)')

if __name__ == '__main__':
    main()
