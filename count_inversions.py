def count(list):
    if len(list) == 1:
        return list, 0

    left, cl = count(list[:len(list) // 2])
    right, cr = count(list[len(list) // 2:])

    return count_split(left, right, cl + cr)

def count_split(left, right, count):
    list = []
    current_j = 0
    current_left_length = len(left)

    for i in left:
        for j in right[current_j:]:
            if j <= i:
                list.append(j)
                current_j += 1
                count += current_left_length
            else:
                break
        list.append(i)
        current_left_length -= 1

    list.extend(right[current_j:])
    return list, count


if __name__ == '__main__':
    from random import randint

    list1 = [randint(-9, 9) for i in range(1000)]
    list2 = [i for i in range(1000, 0, -1)]

    sorted_list1, inversions1 = count(list1)
    sorted_list2, inversions2 = count(list2)

    print(inversions1, "inversions in a random list of", len(list1))
    print(inversions2, "inversions in the worst case list of", len(list2))
    if inversions2 != sum(range(len(list2))):
        print(False)
