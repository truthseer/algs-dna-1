from sys import argv
import scc

def to_points(links):
    points = {}
    for point1, point2 in links:
        if point1 not in points:
            points[point1] = []
        if point2 not in points:
            points[point2] = []

        points[point1].append(point2)
        points[point2].append(point1)
    return points

print('building...')
with open(argv[1]) as f:
    links = []
    for line in f:
        link = tuple(map(int, line.split()))
        links.append(link)

points = scc.to_points(links)
print('finding...')
answers = scc.find(points)
number_of_scc = len(answers)
answers.extend([[] for i in range(5 - len(answers))])
sizes = list(map(str, sorted(map(len, answers), reverse=True)))
print('number of scc =', number_of_scc)
print('answer:', ','.join(sizes[:5]))
