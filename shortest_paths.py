def find(graph):
    start = 1
    frontier = [start]
    path_lengths = {start: 0}

    while frontier:
        min_path = (None, None, 1000000)
        remove = []
        for point1 in frontier:
            i = 0
            for point2 in graph[point1]:
                if point2 in path_lengths:
                    i += 1
                    continue
                length = path_lengths[point1] + graph[point1][point2]
                if length < min_path[2]:
                    min_path = (point1, point2, length)
            if i == len(graph[point1]):
                remove.append(point1)

        for i in remove:
            frontier.remove(i)
        if min_path[1] is not None:
            frontier.append(min_path[1])
        path_lengths[min_path[1]] = min_path[2]

    return path_lengths
