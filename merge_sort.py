def sort(list):
    if len(list) == 1:
        return list

    left = sort(list[:len(list) // 2])
    right = sort(list[len(list) // 2:])

    return merge(left, right)

def merge(left, right):
    list = []
    current_j = 0

    for i in left:
        for j in right[current_j:]:
            if j <= i:
                list.append(j)
                current_j += 1
            else:
                break
        list.append(i)

    list.extend(right[current_j:])
    return list


if __name__ == '__main__':
    from random import randint

    list = [randint(-900, 900) for i in range(100000)]
    sorted_list = sort(list)
    if sorted(list) == sorted_list:
        print(True)
    else:
        print(False)
    #print(sorted_list)
