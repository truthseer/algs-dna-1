from sys import argv
from karger_min_cut import min_cut

with open(argv[1]) as f:
    connection_list = []
    for line in f:
        split = list(map(int, line.split()))
        connection_list.append((split[0], split[1:]))

print('min cut =', min_cut(connection_list))
